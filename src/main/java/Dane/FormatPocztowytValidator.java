package Dane;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import Dane.DaneAdresowe.FormatPocztowy;






public class FormatPocztowytValidator implements ConstraintValidator<FormatPocztowy, String> {
	 
    @Override
    public void initialize(FormatPocztowy format) {
 
    }
 
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value.isEmpty()) 
        {														//sprawdzenie czy warto�� jest pusta
            return false;
        }

        String wzor = "^[0-9]{2}-[0-9]{3}$";					//Format w jakim powinien by� zapisany kod pocztowy
 
        Pattern pattern = Pattern.compile(wzor);				//wzor zostaje skompilowany do instancji klasy Pattern
        Matcher matcher = pattern.matcher(value);				//wynikowy wz�r wykorzystywany jest do utworzenia obiektu machera
        														//kt�ry dopasowuje sekwencje znak�w do wyra�enia 
 
        boolean isValid = matcher.matches();					//Ta metoda kompiluje wyra�enie i dopasowywuje do niego sekwencj� wejscow�
       
 
        return isValid;
    }
}