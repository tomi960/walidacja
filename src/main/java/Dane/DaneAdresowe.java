package Dane;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.*;




public class DaneAdresowe {
	

	@Min(1)
	private int nr_budynku;
	
	@NotBlank(message= "Nazwa ulicy nie mo�e by� pusta")
	private String ulica;
	
	@NotBlank(message= "Warto�� kodu pocztowego nie mo�e by� pusta")
	@FormatPocztowy																			
	private String kod_pocztowy;
	
	@NotBlank(message= "Nazwa miasta nie mo�e by� pusta")
	@WielkaLitera
	private String miasto;

	DaneAdresowe(@Min(1) int nrBudynku, @NotBlank String ulica, String kodPocztowy, @NotBlank String miasto)
	{
		this.nr_budynku = nrBudynku;
		this.ulica = ulica;
		this.kod_pocztowy = kodPocztowy;
		this.miasto = miasto;
	}

	public int getNrBudynku() 
	{
		return nr_budynku;
	}
	
	public String getUlica()
	{
		return ulica;
	}

	public String getKodPocztowy()
	{
		return kod_pocztowy;
	}
	
	public String getMiasto()
	{
		return miasto;
	}
	
	
	@Target({ElementType.FIELD})									//adnotacja mo�e by� u�ywana tylko dla atrybut�w klasy
	@Retention(value = RetentionPolicy.RUNTIME)						//adnotacja @FormatPocztowy b�dzie zachowywanana w VM w trakcie wykonywania programu
	@Constraint(validatedBy = FormatPocztowytValidator.class)		//adnotacja wykorzystywana jest do walidacji i korzysta z walidatora w klasie FormatPocztowyValidator
	public @interface FormatPocztowy {
	 
	    Class<?>[] groups() default {};
	 
	    Class<? extends Payload>[] payload() default { };
	 
	    String message() default "W�a�ciwy format: dd-ddd";
	}
	
	
	@Target({ElementType.FIELD})									//adnotacja mo�e by� u�ywana tylko dla atrybut�w klasy
	@Retention(value = RetentionPolicy.RUNTIME)						//adnotacja @FormatPocztowy b�dzie zachowywanana w VM w trakcie wykonywania programu
	@Constraint(validatedBy = WielkaLiteraValidator.class)			//adnotacja wykorzystywana jest do walidacji i korzysta z walidatora w klasie WielkaLiteraValidator
	public @interface WielkaLitera {
	 
	    Class<?>[] groups() default {};
	 
	    Class<? extends Payload>[] payload() default { };
	 
	    String message() default "Nazwa miasta z ma�ej litery";
	}
	
	
}
