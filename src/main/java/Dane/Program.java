package Dane;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

public class Program {

	public static void main(String[] args) {
		
		  	ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
	        Validator validator = factory.getValidator();														//pobranie instancji walidatora
	        
	        
	        DaneAdresowe Dane = new DaneAdresowe(8, "Wankowicza","53-100", "Wroclaw");	
	        
	        
	        Set<ConstraintViolation<DaneAdresowe>> errors = validator.validate(Dane);							//uruchomienie walidacji dla obiektu Dane i przypisanie b�ed�w do zbioru
	        errors.forEach(error -> System.err.println(error.getPropertyPath() + " " + error.getMessage()));	//wypisanie b�ed�w, kt�re zglosily walidatory
	

		
	}   
}
