package Dane;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import Dane.DaneAdresowe.WielkaLitera;

public class WielkaLiteraValidator  implements ConstraintValidator<WielkaLitera, String> {

    @Override
    public void initialize(WielkaLitera format) {
 
    }
 
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value.isEmpty()) 																//sprawdzenie czy obiekt nie jest pusty
        {
            return false;
        }
 
        boolean isValid = Character.isUpperCase(value.charAt(0));							//sprawdzenie czy pierwsza litera miasta napisana jest z wielkiej litery
 
        return isValid;
    }
	
}
